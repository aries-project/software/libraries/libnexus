set(SOURCES
	"${PROJECT_SOURCE_DIR}/src/thread/event.cpp"

	"${PROJECT_SOURCE_DIR}/src/http/client.cpp"
	"${PROJECT_SOURCE_DIR}/src/http/mock/client.cpp"

	"${PROJECT_SOURCE_DIR}/src/utils/os.cpp"
	"${PROJECT_SOURCE_DIR}/src/utils/string.cpp"
	"${PROJECT_SOURCE_DIR}/src/utils/json.cpp"

	"${PROJECT_SOURCE_DIR}/src/parser/env.cpp"

	"${PROJECT_SOURCE_DIR}/src/wamp/session.cpp"
	"${PROJECT_SOURCE_DIR}/src/wamp/mock/session.cpp"

	"${PROJECT_SOURCE_DIR}/src/wamp/backend_service.cpp"
	"${PROJECT_SOURCE_DIR}/src/wamp/router.cpp"
	"${PROJECT_SOURCE_DIR}/src/wamp/auth/anonymous.cpp"
	"${PROJECT_SOURCE_DIR}/src/wamp/auth/wampcra.cpp"
	"${PROJECT_SOURCE_DIR}/src/wamp/auth/ticket.cpp"
	)

set(LIB_NAME nexus)

add_compile_options(-Wall -Wextra -pedantic)

set(SHARED_TARGET nexus)
add_library(${SHARED_TARGET} SHARED ${SOURCES})
target_include_directories (${SHARED_TARGET} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

set_target_properties(${SHARED_TARGET} PROPERTIES VERSION ${PROJECT_API_VERSION})
set_target_properties(${SHARED_TARGET} PROPERTIES OUTPUT_NAME ${LIB_NAME} CLEAN_DIRECT_OUTPUT 1)

set_property(TARGET ${SHARED_TARGET} PROPERTY CXX_STANDARD 14)
set_property(TARGET ${SHARED_TARGET} PROPERTY CXX_STANDARD_REQUIRED ON)

add_definitions(${WAMPCC_CFLAGS} ${WAMPCC_CFLAGS_OTHER})
target_link_libraries (${SHARED_TARGET} ${WAMPCC_LIBRARIES})
target_link_libraries(${SHARED_TARGET} curl ${CMAKE_THREAD_LIBS_INIT})
target_include_directories(
	${SHARED_TARGET}
	PUBLIC ${3RDPARTY_SOURCE_DIR})

install(TARGETS ${SHARED_TARGET} DESTINATION lib)
