#include <nexus/wamp/backend_service.hpp>
#include <nexus/parser/env.hpp>
#include "jwt/jwt.hpp"

using namespace nexus;
using namespace wampcc;

BackendService::BackendService(const std::string& name, const std::string& env_file, unsigned workers, bool use_log) 
	: WampSession(workers, use_log)
	, m_name(name)
	, m_alg("hs256")
{
	try {
		auto env = parse_env_file(env_file);

		auto value = env.find("JWT_SECRET");
		if(value != env.end())
			setKey(value->second);

	} catch(const std::exception& e) {
		std::cout << e.what() << std::endl;
	}

	http.setHeader("Accept", "application/json");
	http.setHeader("Content-Type", "application/json");
	http.setHeader("charsets", "utf-8");

	http.before_method_cb = [this]() {
		updateToken();
	};
}

void BackendService::setKey(const std::string& key) {
	m_key = key;
}

void BackendService::updateToken() {
	using namespace jwt::params;

	auto iat_tp = std::chrono::system_clock::now();

	if(iat_tp < m_token_expires_at - std::chrono::seconds{30})
		return;

	auto iat = std::chrono::duration_cast<std::chrono::seconds>(iat_tp.time_since_epoch()).count();

	auto exp_tp = iat_tp + std::chrono::minutes{3};
	auto exp = std::chrono::duration_cast<std::chrono::seconds>(exp_tp.time_since_epoch()).count();

	jwt::jwt_object obj{algorithm(m_alg), secret(m_key), payload({
		{"rle", "BACKEND"}
	})};

    obj.add_claim("iss", "backend-jwt")
       .add_claim("sub", 0)
       .add_claim("iat", iat)
       .add_claim("exp", exp)
       ;

    std::string token = obj.signature();

    m_token_expires_at = exp_tp;
	m_token = obj.signature();

	http.setHeader("Authorization", "Bearer " + m_token);
}

void BackendService::onConnect() {
	join( realm(), {"token"}, m_name);
}

std::string BackendService::onChallenge(
	const std::string& /*user*/,
	const std::string& /*authmethod*/,
	const wampcc::json_object& /*extra*/)
{
	updateToken();
	return m_token;
}