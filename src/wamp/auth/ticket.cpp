#include <nexus/wamp/auth/ticket.hpp>

using namespace nexus;
using namespace wampcc;

WampTicket::WampTicket(const std::string& provider_name)
	: WampAuth(provider_name)
{

}

auth_provider WampTicket::provider() {
	auto auth = auth_provider {
		// provider_name
		[this](const std::string&){
			return m_provider_name;
		},
		// policy
		[this](const std::string& user, const std::string& realm) -> wampcc::auth_provider::auth_plan {
			//return auth_provider::auth_plan{ auth_provider::mode::open, {} };
			return onPolicy(user, realm);
		},
		// cra_salt
		nullptr,
		// check_cra
		nullptr,
		// user_secret
		nullptr,
		// check_ticket
		nullptr,
		// user_role
		nullptr,
		// authorize
		[this](const wampcc::t_session_id& /*session_id*/,
				const std::string& realm,
                const std::string& authrole,
                const std::string& uri,
                auth_provider::action action) -> auth_provider::authorized {
			return onAuthorize(realm, authrole, uri, action);
		},
		// authenticate
		[this](const std::string& authid, // not an error, it's authid, realm in wampcc
                const std::string& realm,
                const std::string& authmethod,
                const std::string& signiture){
			return onAuthenticate(realm, authid, authmethod, signiture);
		},
		// hello
		nullptr,
	};

	return std::move(auth);
}

auth_provider::auth_plan WampTicket::onPolicy(const std::string& /*authid*/, const std::string& /*realm*/) {
	return auth_provider::auth_plan(auth_provider::mode::authenticate, {"ticket"} );
}

auth_provider::authorized WampTicket::onAuthorize(const std::string& /*realm*/, 
	const std::string& /*authrole*/, 
	const std::string& /*uri*/, auth_provider::action /*action*/) 
{
	return {true, "", auth_provider::disclosure::optional};
}
