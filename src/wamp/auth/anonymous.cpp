#include <nexus/wamp/auth/anonymous.hpp>

using namespace nexus;
using namespace wampcc;

WampAnonymous::WampAnonymous(const std::string& provider_name)
	: WampAuth(provider_name)
{

}

auth_provider WampAnonymous::provider() {
	return auth_provider {
		// provider_name
		[this](const std::string&){
			return m_provider_name;
		},
		// policy
		[](const std::string&, const std::string&) {
			return auth_provider::auth_plan{ auth_provider::mode::open, {} };
		},
		// cra_salt,
		nullptr,
		// check_cra
		nullptr,
		// user_secret
		nullptr,
		// check_ticket
		nullptr,
		// user_role
		nullptr,
		// authorize
		nullptr,
		// authenticate
		nullptr,
		// hello
		nullptr
	};
}