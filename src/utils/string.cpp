#include <algorithm>
#include <nexus/utils/string.hpp>
#include <iterator>
#include <sstream>

using namespace nexus;

std::vector<std::string> string_utils::split(const std::string& in, char sep)
{
    std::string::size_type b = 0;
    std::vector<std::string> result;

    while ((b = in.find_first_not_of(sep, b)) != std::string::npos) {
        auto e = in.find_first_of(sep, b);
        result.push_back(in.substr(b, e - b));
        b = e;
    }
    return result;
}

std::string string_utils::join(const std::vector<std::string>& elements,
                             std::string separator,
                             std::string prefix,
                             std::string suffix)
{
    std::string result;

    for (auto it = elements.begin(); it != elements.end(); ++it) {
        if (std::next(it) == elements.end()) {
            result += prefix + *it + suffix;
        } else {
            result += prefix + *it + suffix + separator;
        }
    }

    return result;
}

void string_utils::ltrim_ref(std::string& s, std::function<int(int)> test)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(test)));
}

void string_utils::rtrim_ref(std::string& s, std::function<int(int)> test)
{
    s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(test)).base(), s.end());
    // std::string t = " \t\n\r\f\v";
    // s.erase(s.find_last_not_of(t) + 1);
}

void string_utils::trim_ref(std::string& s, std::function<int(int)> test)
{
    string_utils::ltrim_ref(s, test);
    string_utils::rtrim_ref(s, test);
}

std::string string_utils::rtrim(std::string s, std::function<int(int)> test)
{
    string_utils::rtrim_ref(s, test);
    return s;
}

std::string string_utils::ltrim(std::string s, std::function<int(int)> test)
{
    string_utils::ltrim_ref(s, test);
    return s;
}

std::string string_utils::trim(std::string s, std::function<int(int)> test)
{
    string_utils::trim_ref(s, test);
    return s;
}