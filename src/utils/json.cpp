#include <nexus/utils/json.hpp>
#include <nexus/utils/string.hpp>

using namespace nexus;

wampcc::json_value& json_utils::getJsonNode(wampcc::json_object& obj,
                                             const std::vector<std::string>& keys,
                                             unsigned index,
                                             bool create)
{
    if (index >= keys.size())
        throw std::out_of_range("index");

    auto key    = keys[index];
    auto& value = obj[key];

    if (value.is_null()) {
        if (create) {
            if (index < keys.size() - 1) {
                value = wampcc::json_object();
                return json_utils::getJsonNode(value.as_object(), keys, index + 1, create);
            } else {
                return value;
            }
        } else {
            throw std::out_of_range("no entry with key \"" + key + "\"");
        }
    } else {
        if (index == keys.size() - 1) {
            return value;
        } else {
            if (value.is_object()) {
                return json_utils::getJsonNode(value.as_object(), keys, index + 1, create);
            } else {
                throw std::out_of_range("no entry with sub-key \"" + key + "\"");
            }
        }
    }
}

void json_utils::setProperty(wampcc::json_object& obj, const std::string& key, const wampcc::json_value& value)
{
    auto keys  = string_utils::split(key, '.');
    auto& node = json_utils::getJsonNode(obj, keys, 0, true);
    node       = value;
}

wampcc::json_value json_utils::getProperty(wampcc::json_object& obj,
                                            const std::string& key,
                                            const wampcc::json_value& fallback)
{
    try {
        auto keys = string_utils::split(key, '.');
        return json_utils::getJsonNode(obj, keys, 0, false);
    } catch (...) {
        return fallback;
    }
}

float json_utils::getNumericProperty(wampcc::json_object& obj, const std::string& key, float fallback)
{
    auto value = json_utils::getProperty(obj, key, fallback);
    if (value.is_integer())
        return value.as_int();
    else if (value.is_real())
        return value.as_real();

    return fallback;
}

void json_utils::setPropertyIfNotExists(wampcc::json_object& obj,
                                         const std::string& key,
                                         const wampcc::json_value& value)
{
    auto keys = string_utils::split(key, '.');
    try {
        json_utils::getJsonNode(obj, keys);
    } catch (...) {
        auto& node = json_utils::getJsonNode(obj, keys, 0, true);
        node       = value;
    }
}

bool json_utils::haveProperty(wampcc::json_object& obj, const std::string& key)
{
    try {
        auto keys = string_utils::split(key, '.');
        json_utils::getJsonNode(obj, keys);
        return true;
    } catch (...) {
        return false;
    }
}