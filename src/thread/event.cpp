#include <functional>
#include <iostream>
#include <nexus/thread/event.hpp>

using namespace nexus;

Event::Event() {
	m_flag = false;
}

bool Event::wait(unsigned timeout) {
	if(m_flag)
		return true;

	std::unique_lock<std::mutex> m_lock(m_mutex);



	if(timeout) {
		m_cond.wait_for(m_lock, std::chrono::milliseconds(timeout), std::bind(&Event::m_flag, this) );
		return m_flag;
	} else {
		m_cond.wait(m_lock, std::bind(&Event::m_flag, this) );
	}

	return true;
}

bool Event::wait_until(const std::chrono::system_clock::time_point &endtime)
{
	if(m_flag)
		return true;

	std::unique_lock<std::mutex> m_lock(m_mutex);
	m_cond.wait_until(m_lock, endtime, std::bind(&Event::m_flag, this));
	return m_flag;
}

void Event::set() {
	m_flag = true;
	m_cond.notify_all();
}

void Event::clear() {
	m_flag = false;
	m_cond.notify_all();
}

bool Event::isSet() {
	return m_flag;
}
