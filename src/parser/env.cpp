#include <nexus/parser/env.hpp>
#include <nexus/utils/string.hpp>
#include <iostream>
#include <fstream>

std::map<std::string,std::string> nexus::parse_env_file(const std::string& filename) {
	std::map<std::string,std::string> env;

	std::ifstream file;
	file.open(filename, std::ifstream::in);

	while(file) {
		std::string line;
		getline(file, line);

		string_utils::trim_ref(line);

		auto values = string_utils::split(line, '=');
		if(values.size() == 2)
			env.insert({values[0], values[1]});
	}

	return std::move(env);
}