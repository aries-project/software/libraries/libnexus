# nexus library headers
set(LIBRARY_HEADERS
	${PROJECT_SOURCE_DIR}/include/nexus/nexus.hpp
	)

install(FILES ${LIBRARY_HEADERS} DESTINATION include/nexus)

set(THREAD_HEADERS
	${PROJECT_SOURCE_DIR}/include/nexus/thread/event.hpp
	${PROJECT_SOURCE_DIR}/include/nexus/thread/queue.hpp
	)

install(FILES ${THREAD_HEADERS} DESTINATION include/nexus/thread)

set(HTTP_HEADERS
	${PROJECT_SOURCE_DIR}/include/nexus/http/client.hpp
	)
install(FILES ${HTTP_HEADERS} DESTINATION include/nexus/http)

set(HTTP_MOCK_HEADERS
	${PROJECT_SOURCE_DIR}/include/nexus/http/mock/client.hpp
	)
install(FILES ${HTTP_MOCK_HEADERS} DESTINATION include/nexus/http/mock)

set(UTILS_HEADERS
	${PROJECT_SOURCE_DIR}/include/nexus/utils/os.hpp
	${PROJECT_SOURCE_DIR}/include/nexus/utils/string.hpp
	${PROJECT_SOURCE_DIR}/include/nexus/utils/json.hpp
	)
install(FILES ${UTILS_HEADERS} DESTINATION include/nexus/utils)

set(PARSER_HEADERS
	${PROJECT_SOURCE_DIR}/include/nexus/parser/env.hpp
	)
install(FILES ${PARSER_HEADERS} DESTINATION include/nexus/parser)

set(WAMP_HEADERS
	${PROJECT_SOURCE_DIR}/include/nexus/wamp/session.hpp
	${PROJECT_SOURCE_DIR}/include/nexus/wamp/promise_pool.hpp
	${PROJECT_SOURCE_DIR}/include/nexus/wamp/backend_service.hpp
	${PROJECT_SOURCE_DIR}/include/nexus/wamp/router.hpp
	${PROJECT_SOURCE_DIR}/include/nexus/wamp/auth.hpp
	${PROJECT_SOURCE_DIR}/include/nexus/wamp/topics.hpp
	)
install(FILES ${WAMP_HEADERS} DESTINATION include/nexus/wamp)

set(WAMP_AUTH_HEADERS
	${PROJECT_SOURCE_DIR}/include/nexus/wamp/auth/anonymous.hpp
	${PROJECT_SOURCE_DIR}/include/nexus/wamp/auth/wampcra.hpp
	${PROJECT_SOURCE_DIR}/include/nexus/wamp/auth/ticket.hpp
	)
install(FILES ${WAMP_AUTH_HEADERS} DESTINATION include/nexus/wamp/auth)

set(WAMP_MOCK_HEADERS
	${PROJECT_SOURCE_DIR}/include/nexus/wamp/mock/session.hpp
	)
install(FILES ${WAMP_MOCK_HEADERS} DESTINATION include/nexus/wamp/mock)
