#ifndef NEXUS_PARSER_ENV_HPP
#define NEXUS_PARSER_ENV_HPP

#include <string>
#include <map>

namespace nexus {

std::map<std::string,std::string> parse_env_file(const std::string& filename);

} // namsepace fabui

#endif /* NEXUS_PARSER_ENV_HPP */