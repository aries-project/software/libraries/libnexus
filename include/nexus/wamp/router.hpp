#ifndef NEXUS_WAMP_ROUTER_HPP
#define NEXUS_WAMP_ROUTER_HPP

#include <functional>
#include <memory>
#include <wampcc/wampcc.h>

namespace nexus {

	typedef std::function<void(wampcc::wamp_session&, wampcc::call_info)> on_call_fn;

	class WampRouter {
		public:
			WampRouter(unsigned port);
			~WampRouter();

			virtual bool listen();
			bool listen(wampcc::auth_provider auth);
			void disconnect();

  			void publish(
  				const std::string& realm,
  				const std::string& uri,
               	const wampcc::json_object& options, 
               	wampcc::wamp_args args);

			void callable(
				const std::string& realm, 
				const std::string& procedure, 
				on_call_fn call_cb
				//,wampcc::json_object options = {}
				);

			virtual void onJoin();
			virtual void onDisconnect();

			/*wampcc::auth_provider::authenticated
				onAuthenticate(const std::string& authid,
                const std::string& realm,
                const std::string& authmethod,
                const std::string& signiture);*/


			std::future<void> finished_future() { return ready_to_exit.get_future(); }
			std::promise<void> ready_to_exit;
		private:
			std::unique_ptr<wampcc::kernel> the_kernel;

		protected:
			std::shared_ptr<wampcc::wamp_router> m_router;
			unsigned m_port;
			bool m_running;
	};

} // namespace nexus

#endif /* NEXUS_WAMP_ROUTER_HPP */