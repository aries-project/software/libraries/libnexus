#ifndef NEXUS_WAMP_SESSION_HPP
#define NEXUS_WAMP_SESSION_HPP

#include <nexus/wamp/promise_pool.hpp>
#include <functional>
#include <future>
#include <memory>
#include <thread>
#include <vector>
#include <wampcc/wampcc.h>

class ThreadPool;

namespace nexus {

typedef std::function<void(const wampcc::invocation_info&)> on_invocation_fn;
typedef std::function<void(const wampcc::event_info&)> on_event_fn;

/// Basic WampSession interface
class IWampSession
{
  public:
    virtual std::future<wampcc::subscribed_info> subscribe(const std::string& topic,
                                                           on_event_fn event_cb,
                                                           wampcc::json_object options = {}) = 0;

    virtual std::future<wampcc::unsubscribed_info> unsubscribe(wampcc::t_subscription_id) = 0;

    // std::future<wampcc::published_info>
    virtual void publish(const std::string& topic, wampcc::wamp_args args, wampcc::json_object options = {}) = 0;

    virtual std::future<wampcc::result_info> call(const std::string& procedure,
                                                  wampcc::wamp_args args      = {},
                                                  wampcc::json_object options = {}) = 0;
};

/// WampSession implementation
class WampSession : public IWampSession
{
  public:
    WampSession(unsigned workers = 0, bool use_log = false);
    ~WampSession();

    void provide(const std::string& procedure, on_invocation_fn invocation_cb, wampcc::json_object options = {});

    std::future<wampcc::subscribed_info> subscribe(const std::string& topic,
                                                   on_event_fn event_cb,
                                                   wampcc::json_object options = {});

    std::future<wampcc::unsubscribed_info> unsubscribe(wampcc::t_subscription_id);

    // std::future<wampcc::published_info>
    void publish(const std::string& topic, wampcc::wamp_args args, wampcc::json_object options = {});

    std::future<wampcc::result_info> call(const std::string& procedure,
                                          wampcc::wamp_args args      = {},
                                          wampcc::json_object options = {});

    void yield(wampcc::t_request_id);
    void yield(wampcc::t_request_id, wampcc::json_array);
    void yield(wampcc::t_request_id, wampcc::json_array, wampcc::json_object);
    void yield(wampcc::t_request_id, wampcc::json_object options);
    void yield(wampcc::t_request_id, wampcc::json_object options, wampcc::json_array);
    void yield(wampcc::t_request_id, wampcc::json_object options, wampcc::json_array, wampcc::json_object);

    void error(wampcc::t_request_id, std::string error);
    void error(wampcc::t_request_id, std::string error, wampcc::json_array);
    void error(wampcc::t_request_id, std::string error, wampcc::json_array, wampcc::json_object);

    virtual bool connect(const std::string& address, unsigned port, const std::string& realm);
    virtual bool connect(const std::string& uri, const std::string& realm);
    virtual void disconnect();

    void submit(std::function<void()>);

    std::future<void> finished_future() { return ready_to_exit.get_future(); }

    std::string realm() { return m_realm; }

    const wampcc::json_value& getArgumentOrFail(unsigned index,
                                                const wampcc::json_array& args_list,
                                                const std::string& error_message = "missing argument at position #{0}");

    const wampcc::json_value& getArgumentOrFail(const std::string& name,
                                                const wampcc::json_object& args_dict,
                                                const std::string& error_message = "missing \"{0}\" argument");

    template<typename T>
    const wampcc::json_value getArgumentOrDefault(const std::string& name,
                                                  const wampcc::json_object& args_dict,
                                                  const T& default_value)
    {
        auto arg = args_dict.find(name);
        if (arg == args_dict.end())
            return default_value;
        return arg->second;
    }

    std::string getCallerIdOrFail(wampcc::json_object& details,
                                  const std::string& error_message = "caller ID not available");

  private:
    void rpc_registered(const std::string& procedure, wampcc::registered_info info);

    std::unique_ptr<wampcc::kernel> the_kernel;
    std::string m_realm;

    std::mutex m_mutex_ws;
    std::promise<void> ready_to_exit;

    std::promise<wampcc::subscribed_info> m_subscribed_promise;
    std::promise<wampcc::unsubscribed_info> m_unsubscribe_promise;
    std::promise<wampcc::published_info> m_published_promise;
    // As multiple concurent calls can happend from dirrefent threads
    // a pool of up to 8 promises is used to syncthe caller and callee
    nexus::PromisePool<wampcc::result_info, 8> m_result_promise_pool;

  protected:
    bool m_use_threadpool;
    std::unique_ptr<ThreadPool> m_workers;

    bool m_connected;
    std::shared_ptr<wampcc::wamp_session> m_session;

    virtual std::string onChallenge(const std::string& user,
                                    const std::string& authmethod,
                                    const wampcc::json_object& extra);

    virtual void onConnect();
    virtual void onJoin() = 0;
    virtual void onDisconnect();

    void join(const std::string& realm);
    void join(const std::string& realm, std::vector<std::string> authmethods);

    void join(const std::string& realm,
              std::vector<std::string> authmethods,
              const std::string& authid,
              const std::string& secret,
              wampcc::json_object authextra = {});

    void join(const std::string& realm,
              std::vector<std::string> authmethods,
              const std::string& authid,
              wampcc::json_object authextra = {});
};

} // namespace nexus

#endif /* NEXUS_WAMP_SESSION_HPP */