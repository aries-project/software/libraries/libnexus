#ifndef NEXUS_WAMP_AUTH_HPP
#define NEXUS_WAMP_AUTH_HPP

#include <wampcc/wampcc.h>

namespace nexus {

class WampAuth {
	public:
		WampAuth(const std::string& provider_name): m_provider_name(provider_name) { };
		~WampAuth() {};

		virtual wampcc::auth_provider provider() =0;
	protected:
		std::string m_provider_name;

};

} // namespace nexus

#endif /* NEXUS_WAMP_AUTH_HPP */