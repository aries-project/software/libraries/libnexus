#ifndef NEXUS_WAMP_AUTH_ANONYMOUS_HPP
#define NEXUS_WAMP_AUTH_ANONYMOUS_HPP

#include <nexus/wamp/auth.hpp>

namespace nexus {

class WampAnonymous: public WampAuth {
	public:
		WampAnonymous(const std::string& provider_name = "anonymous");
		wampcc::auth_provider provider();
};

} // namespace nexus

#endif /* NEXUS_WAMP_AUTH_ANONYMOUS_HPP */