#ifndef NEXUS_WAMP_AUTH_WAMPCRA_HPP
#define NEXUS_WAMP_AUTH_WAMPCRA_HPP

#include <nexus/wamp/auth.hpp>

namespace nexus {

class WampWampcra: public WampAuth {
	public:
		WampWampcra(const std::string& provider_name = "wampcra");
		wampcc::auth_provider provider();
};

} // namespace fabui

#endif /* NEXUS_WAMP_AUTH_WAMPCRA_HPP */