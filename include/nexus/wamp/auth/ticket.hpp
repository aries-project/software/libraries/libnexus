#ifndef NEXUS_WAMP_AUTH_TICKET_HPP
#define NEXUS_WAMP_AUTH_TICKET_HPP

#include <nexus/wamp/auth.hpp>

namespace nexus {

class WampTicket: public WampAuth {
	public:
		WampTicket(const std::string& provider_name = "ticket");
		wampcc::auth_provider provider();

		virtual wampcc::auth_provider::authenticated
			onAuthenticate(
				const std::string& realm,
                const std::string& authid,
                const std::string& authmethod,
                const std::string& ticket
			) =0;

		virtual wampcc::auth_provider::authorized
			onAuthorize(
				const std::string& realm,
                const std::string& authrole,
                const std::string& uri,
                wampcc::auth_provider::action
			);

		virtual wampcc::auth_provider::auth_plan 
			onPolicy(
				const std::string& authid, 
				const std::string& realm
			);
};

} // namespace nexus

#endif /* NEXUS_WAMP_AUTH_TICKET_HPP */