#ifndef NEXUS_WAMP_BACKEND_SERVICE_HPP
#define NEXUS_WAMP_BACKEND_SERVICE_HPP

#include <chrono>
#include <nexus/wamp/session.hpp>
#include <nexus/http/client.hpp>

namespace nexus {

class BackendService : public nexus::WampSession {
	public:
		BackendService(const std::string& name, const std::string& env_file, unsigned workers = 0, bool use_log = false);
		
		HttpClient http;
	protected:
		void updateToken();
		void onConnect();
		std::string onChallenge(const std::string& user, const std::string& authmethod,	const wampcc::json_object& extra);
	private:
		std::string m_name;
		std::string m_key;
		std::string m_alg;

		std::string m_token;
		std::chrono::system_clock::time_point m_token_expires_at;

		void setKey(const std::string& key);
};

} // namespace nexus

#endif /* NEXUS_WAMP_BACKEND_SERVICE_HPP */