#ifndef WAMP_FABUI_TOPICS_HPP
#define WAMP_FABUI_TOPICS_HPP

#define TOPIC_GCODE 		"gcode.events"
#define TOPIC_GCODE_FILE	"gcode.file.events"

#define TOPIC_TASK          "task.events"

#endif /* WAMP_FABUI_TOPICS_HPP */
