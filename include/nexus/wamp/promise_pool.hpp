#pragma once

#include <functional>
#include <future>
#include <list>
#include <memory>
#include <tuple>

namespace nexus {

template<typename T, unsigned SIZE>
class PromisePool
{
  private:
    std::promise<T> m_promises[SIZE];
    unsigned m_index = 0;

  public:
    PromisePool() {}

    std::promise<T>& getPromise()
    {
        unsigned next = m_index;
        m_index++;
        m_index          = m_index % SIZE;
        m_promises[next] = std::promise<T>();
        return m_promises[next];
    }
};

} /* namespace nexus */
