#ifndef NEXUS_EVENT_HPP
#define NEXUS_EVENT_HPP

#include <mutex>
#include <condition_variable>
#include <chrono>

namespace nexus {

class Event {
	/**
	 * C++ equivalent of python Event object 
	 */
	private:
		std::mutex m_mutex;
		std::condition_variable m_cond;
		//std::unique_lock<std::mutex> m_lock;
	public:
		bool m_flag; // The internal flag is initially false

		Event();

		/**
		 * Block until the internal flag is true. If the internal flag is true on entry, return immediately. 
		 * Otherwise, block until another thread calls set() to set the flag to true, or until the optional timeout occurs.
		 *
		 * @param timeout Wait timeout in milliseconds
		 */
		bool wait(unsigned timeout=0);


		bool wait_until(const std::chrono::system_clock::time_point &endtime);
		/**
		 * Set the internal flag to true. All threads waiting for it to become true are awakened.
		 * Threads that call wait() once the flag is true will not block at all.
		 */
		void set();

		/**
		 * Reset the internal flag to false. Subsequently, threads calling wait() will block until set() 
		 * is called to set the internal flag to true again.
		 */
		void clear();

		/**
		 * Return true if and only if the internal flag is true.
		 *
		 * @returns internal flag value
		 */
		bool isSet();
};

} // namespace nexus

#endif /* NEXUS_EVENT_HPP */