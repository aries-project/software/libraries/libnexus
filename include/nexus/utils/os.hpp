#ifndef NEXUS_OS_UTILS_HPP
#define NEXUS_OS_UTILS_HPP

#include <memory>
#include <string>
#include <vector>

namespace nexus {
namespace os_utils {

    std::vector<std::string> getDirectoryFiles(const std::string& dir_name);
    bool copyFiles(const std::string& srcDir, const std::string& dstDir);
    bool copyFile(const std::string& srcFile, const std::string& dstFile);
    bool createSoftlink(const std::string& srcFile, const std::string& dstFile, bool overwrite = true);
    bool createFile(const std::string& filename, const std::string& content = "", bool append = false);
    std::string getFileContent(const std::string& filename);
    bool changeMode(const std::string& filename, unsigned mode);
    //
    bool exists(const std::string& path);
    bool remove(const std::string& path);
    bool removeDirectory(const std::string& path);
    // directory
    bool mkDir(const std::string& dirname, bool recursive = false, unsigned mode = 0755);
    bool isDirectory(const std::string& path);
    bool isFile(const std::string& path);
    bool isExecutable(const std::string& path);
    // path
    std::string pathJoin(std::initializer_list<std::string> paths);
    std::string baseName(const std::string& filename);
    std::string dirName(const std::string& filename);
    std::string fileExtension(const std::string& filename);
    // process
    std::string execute(const std::string& command, unsigned* exitCode = nullptr);
    bool executeSimple(const std::string& command, unsigned successExitCode = 0);

} // namespace os_utils
} // namespace nexus

#endif /* NEXUS_OS_UTILS_HPP */