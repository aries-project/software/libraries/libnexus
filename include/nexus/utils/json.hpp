#ifndef NEXUS_JSON_UTILS_HPP
#define NEXUS_JSON_UTILS_HPP

#include <cstdbool>
#include <string>
#include <wampcc/json.h>

namespace nexus {
namespace json_utils {
    wampcc::json_value& getJsonNode(wampcc::json_object& obj,
                                    const std::vector<std::string>& keys,
                                    unsigned index = 0,
                                    bool create    = false);

    void setProperty(wampcc::json_object& obj, const std::string& key, const wampcc::json_value& value);

    wampcc::json_value getProperty(wampcc::json_object& obj,
                                   const std::string& key,
                                   const wampcc::json_value& fallback = false);

    float getNumericProperty(wampcc::json_object& obj, const std::string& key, float fallback = 0.0);

    void setPropertyIfNotExists(wampcc::json_object& obj, const std::string& key, const wampcc::json_value& value);

    bool haveProperty(wampcc::json_object& obj, const std::string& key);

} // namespace json_utils
} // namespace nexus

#endif /* NEXUS_JSON_UTILS_HPP */