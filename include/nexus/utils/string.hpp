#ifndef NEXUS_STRING_UTILS_HPP
#define NEXUS_STRING_UTILS_HPP

// #include <cctype>
#include <functional>
#include <memory>
#include <string>
#include <vector>

namespace nexus {
namespace string_utils {

    std::vector<std::string> split(const std::string& in, char sep = ' ');

    std::string join(const std::vector<std::string>& elements,
                     std::string separator = "",
                     std::string prefix    = "",
                     std::string suffix    = "");

    void trim_ref(std::string&, std::function<int(int)> test = std::ptr_fun<int, int>(std::isspace));
    void rtrim_ref(std::string&, std::function<int(int)> test = std::ptr_fun<int, int>(std::isspace));
    void ltrim_ref(std::string&, std::function<int(int)> test = std::ptr_fun<int, int>(std::isspace));

    std::string trim(std::string, std::function<int(int)> test = std::ptr_fun<int, int>(std::isspace));
    std::string rtrim(std::string, std::function<int(int)> test = std::ptr_fun<int, int>(std::isspace));
    std::string ltrim(std::string, std::function<int(int)> test = std::ptr_fun<int, int>(std::isspace));

} // namespace string_utils
} // namespace nexus

#endif /* NEXUS_STRING_UTILS_HPP */