/**
 * Copyright (C) 2020 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: client.hpp
 * @brief HttpClient mock definition
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#ifndef NEXUS_HTTP_CLIENT_MOCK_H
#define NEXUS_HTTP_CLIENT_MOCK_H

#include <nexus/http/client.hpp>
#include <forward_list>

namespace nexus {

	typedef std::function<HttpReponse(HttpMethod http_method, 
				const std::string& sub_url,
				const std::string& data,
				const IHttpClient::params_t& params,
				const IHttpClient::headers_t& headers)> on_method_fn;

	struct HttpRequestMock {
		std::string sub_url;
		on_method_fn method; 
	};

	class HttpClientMock: public IHttpClient {
		public:
			HttpClientMock();
			virtual ~HttpClientMock();

			/**
			 * 
			 */	
			HttpReponse method(HttpMethod http_method, 
				const std::string& sub_url, 
				const wampcc::json_value& json_data = {},
				const params_t& params = {},
				const headers_t& headers = {});

			/**
			 * 
			 */	
			HttpReponse method(HttpMethod method, 
				const std::string& sub_url, 
				const std::string& data,
				const params_t& params = {},
				const headers_t& headers = {});

			/**
			 *
			 */
			HttpReponse get(const std::string& sub_url, const params_t& params = {}, const headers_t& headers = {});

			/**
			 *
			 */
			HttpReponse post(const std::string& sub_url, const wampcc::json_value& json_data, const params_t& params = {}, const headers_t& headers = {});

			/**
			 *
			 */
			HttpReponse put(const std::string& sub_url, const wampcc::json_value& json_data, const params_t& params = {}, const headers_t& headers = {});

			/**
			 *
			 */
			HttpReponse delete1(const std::string& sub_url, const params_t& params = {}, const headers_t& headers = {});


			/**
			 *
			 */
			void addResponder(const std::string& sub_url, on_method_fn method);
		private:
			std::forward_list<HttpRequestMock> m_responder;

	};

} /* fabui */

#endif /* NEXUS_HTTP_CLIENT_MOCK_H */