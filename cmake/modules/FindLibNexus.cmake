# This module tries to find libserialxx library and include files
#
# LIBNEXUS_INCLUDE_DIR, path where to find libwebsockets.h
# LIBNEXUS_LIBRARY_DIR, path where to find libwebsockets.so
# LIBNEXUS_LIBRARIES, the library to link against
# LIBNEXUS_FOUND, If false, do not try to use libWebSockets
#
# This currently works probably only for Linux

FIND_PATH ( LIBNEXUS_INCLUDE_DIR nexus/nexus.hpp
    /usr/local/include
    /usr/include
)

FIND_LIBRARY ( LIBNEXUS_LIBRARIES fabui
    /usr/local/lib
    /usr/lib
)

GET_FILENAME_COMPONENT( LIBNEXUS_LIBRARY_DIR ${LIBNEXUS_LIBRARIES} PATH )

SET ( LIBNEXUS_FOUND "NO" )
IF ( LIBNEXUS_INCLUDE_DIR )
    IF ( LIBNEXUS_LIBRARIES )
        SET ( LIBNEXUS_FOUND "YES" )
    ENDIF ( LIBNEXUS_LIBRARIES )
ENDIF ( LIBNEXUS_INCLUDE_DIR )

MARK_AS_ADVANCED(
    LIBNEXUS_LIBRARY_DIR
    LIBNEXUS_INCLUDE_DIR
    LIBNEXUS_LIBRARIES
)
