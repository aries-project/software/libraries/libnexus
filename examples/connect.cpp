#include <iostream>
#include <nexus/wamp/session.hpp>

using namespace nexus;

class A: public WampSession {
	public:
		A(): WampSession() {}

		void onJoin() {
			std::cout << "Joined\n";
			disconnect();
		}

		void onDisconnect() {
			std::cout << "Disconnected\n";
		}
};

int main() {
	A a;
	a.connect("127.0.0.1", 9000, "nexus");
	return 0;
}