#include <iostream>
#include <nexus/wamp/session.hpp>

using namespace nexus;
using namespace wampcc;
using namespace std::placeholders;

class A: public WampSession {
	public:
		A(): WampSession(0) {}

		
		void terminate(invocation_info info) {
			std::cout << "id: " << info.request_id << std::endl;
			std::cout << "rid: " << info.registration_id << std::endl;
			std::cout << "details: " << info.details << std::endl;
			yield(info.request_id);
			//disconnect();
		}

		void onJoin() {
			std::cout << "Joined\n";
			provide("nexus.test.terminate", std::bind(&A::terminate, this, _1));
		}

		void onDisconnect() {
			std::cout << "Disconnected\n";
		}
};

int main() {
	A a;
	a.connect("127.0.0.1", 9000, "nexus");
	return 0;
}