#include <iostream>
#include <nexus/wamp/router.hpp>
#include <nexus/wamp/auth.hpp>
#include <jwt/jwt.hpp>

using namespace nexus;
using namespace wampcc;
using namespace std::placeholders;

class TokenAuth : public WampAuth {
	public:
		TokenAuth()
			: WampAuth("token")
		{

		}

		auth_provider provider() {
			auto auth = auth_provider {
				// provider_name
				[this](const std::string&){
					return m_provider_name;
				},
				// policy
				[this](const std::string& authid, const std::string& realm) {
					//return auth_provider::auth_plan{ auth_provider::mode::open, {} };
					return onPolicy(authid, realm);
				},
				// cra_salt
				nullptr,
				// cra_check
				nullptr,
				// user_secret
				nullptr,
				// check_ticket
				nullptr,
				// user_role
				nullptr,
				// authorize
				[this]( const wampcc::t_session_id& /*session_id*/,
						const std::string& realm,
		                const std::string& authrole,
		                const std::string& uri,
		                auth_provider::action action) {
					return onAuthorize(realm, authrole, uri, action);
				},
				// authenticate
				[this](	const std::string& authid, // not an error, it's authid, realm in wampcc
		                const std::string& realm,
		                const std::string& authmethod,
		                const std::string& signiture) {
					return onAuthenticate(realm, authid, authmethod, signiture);
				},
				// hello
				[this](	const std::string& authid,
                		const std::string& realm,
                		const std::string& authmethod,
                		const std::string& authprovider,
                		t_session_id session) {
					return json_object();
				}
                
			};

			return std::move(auth);
		}

		auth_provider::auth_plan onPolicy(
				const std::string& authid, 
				const std::string& realm) 
		{
			return auth_provider::auth_plan(auth_provider::mode::authenticate, {"token"} );
		}

		auth_provider::authenticated onAuthenticate(const std::string& realm, 
				const std::string& authid,
                const std::string& authmethod,
                const std::string& token) 
		{
			std::cout << "Authenticated: (" << authid << ", " << token << ") on " << realm << std::endl;
			using namespace jwt::params;
			auto key = "secret_key";

			try {
				auto dec_obj = jwt::decode(token, algorithms({"hs256"}), secret(key));
	  			std::cout << dec_obj.header() << std::endl;
	  			std::cout << dec_obj.payload() << std::endl;

	  			return {
	  				true,
	  				"api",
	  				authid
	  			};
			} catch(const std::exception& e) {
				std::cout << "jwt-decode failed: " << e.what() << std::endl;
			}

			std::cout << "Authentication denied\n";
			return {false};
		}

		auth_provider::authorized onAuthorize(
				const std::string& realm,
                const std::string& authrole,
                const std::string& uri,
                wampcc::auth_provider::action)
		{
			return {true, "", auth_provider::disclosure::always};
		}
};

class A: public WampRouter {
	private:
		TokenAuth token;
	public:
		A(unsigned port) 
			: WampRouter(port)
		{

		}

		void terminate(wamp_session &caller, call_info info) {
			std::cout << "terminated\n";
			caller.result(info.request_id);
			disconnect();
		}

		void onJoin() {
			callable(
				"nexus", 
				"nexus.router.terminate",
				std::bind(&A::terminate, this, _1, _2)
				);
		}

		void onDisconnect() {
			std::cout << "Disconnecting...\n";
		}

		bool listen() {
			auth_provider auth = token.provider();
			return WampRouter::listen(auth);
		}
};

int main() {
	A a(9000);

	if( a.listen() ) {
		auto finished = a.finished_future();
		finished.wait();
	}
	return 0;
}