#include <iostream>
#include <nexus/wamp/session.hpp>

using namespace nexus;

class A: public WampSession {
	public:
		A(): WampSession() {}


		std::string onChallenge(
			const std::string& user,
			const std::string& authmethod,
			const wampcc::json_object& extra)
		{
			return "eyJ0eXAiOiJ4eHgiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJmYWJ1aS1qd3QiLCJzdWIiOjEsInJsZSI6IkFETUlOIiwiaWF0IjoxNTM1NjIxMjQ5LCJleHAiOjE1MzU2MjQ4NDl9.d0wXjJJRR_TqjDHR1fYojY8O58WjpbnalXXUMl68hGE";
		}

		void onJoin() {
			std::cout << "Joined\n";
			disconnect();

		}

		void onConnect() {
			std::cout << "onConnect\n";
			join( realm(), {"token"}, "token");
		}

		void onDisconnect() {
			std::cout << "Disconnected\n";
		}
};

int main() {
	A a;
	if( a.connect("127.0.0.1", 9000, "nexus") ) {
		std::cout << "get future\n" << std::flush;
		auto finished = a.finished_future();
		finished.wait();	
	}

	//a.connect("ws://127.0.0.1:55555", "private_realm");

	return 0;
}