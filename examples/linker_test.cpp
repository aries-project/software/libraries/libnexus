#include <nexus/wamp/session.hpp>

using namespace nexus;

class A: public WampSession {
	public:
		A(): WampSession() {}

		void onJoin() {}
};

int main() {
	A a;
	return 0;
}