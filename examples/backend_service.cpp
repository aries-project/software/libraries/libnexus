#include <nexus/wamp/backend_service.hpp>

using namespace nexus;
using namespace wampcc;

class A: public BackendService {
	public:
		A() 
			: BackendService("example", "/mnt/projects/Colibri-Embedded/fabui/fabui-frontend/application/.env")
		{

		}

		void onJoin() {

		}
};

int main() {
	A a;
	if( a.connect("ws://127.0.0.1:9000", "nexus")) {
		auto finished = a.finished_future();
		finished.wait();
	}
}